import { Component, OnInit } from '@angular/core';
import { FormGroup, Validator, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signUp: FormGroup | undefined;
  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.signUp = this.fb.group({
      userName: ['', Validators.required, Validators.email],
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required]]
    });
  }

}
