import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public columns: any;
  public rows: any;

  columnDefs = [
    { headerName: 'Employee ID', field: 'employeeID' },
    { headerName: 'Employee Name', field: 'employeeName' },
    { headerName: 'Designation', field: 'designation' },
    { headerName: 'Date Of Joining', field: 'dateOfJoining' }
  ];

  rowData = [
    { employeeID: '10213', employeeName: 'Celica', designation: 'Software Engineer', dateOfJoining: '01/02/2019' },
    { employeeID: '10289', employeeName: 'Mondeo', designation: 'Quality engineer', dateOfJoining: '01/02/2019' },
    { employeeID: '12092', employeeName: 'Boxter', designation: 'Manager', dateOfJoining: '01/02/2019' }
  ];
  constructor() {
    this.columns = [
      { headerName: 'Employee ID', field: 'employeeID' },
      { headerName: 'Employee Name', field: 'employeeName' },
      { headerName: 'Designation', field: 'designation' },
      { headerName: 'Date Of Joining', field: 'dateOfJoining' }
    ];
    this.rows = [
      { employeeID: '10213', employeeName: 'Celica', designation: 'Software Engineer', dateOfJoining: '01/02/2019' },
      { employeeID: '10289', employeeName: 'Mondeo', designation: 'Quality engineer', dateOfJoining: '01/02/2019' },
      { employeeID: '12092', employeeName: 'Boxter', designation: 'Manager', dateOfJoining: '01/02/2019' }
    ];
  }

  ngOnInit(): void {
  }

}
