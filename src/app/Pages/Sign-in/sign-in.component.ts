import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  constructor(
    private router: Router,
    private toaster: ToastrService
  ) { }

  ngOnInit(): void {
  }
  getSignUpPage() {
    debugger
    this.router.navigate(['/app-sign-up']);
  }

}
