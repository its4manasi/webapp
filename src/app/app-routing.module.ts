import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './Pages/Sign-in/sign-in.component';
import { SignUpComponent } from './Pages/Sign-up/sign-up.component';
import { DashboardComponent } from './Pages/Dashboard/dashboard.component';


const routes: Routes = [
  {
    path: 'app-sign-in', component: SignInComponent
  },
  {
    path: 'app-sign-up', component: SignUpComponent, pathMatch: 'full',
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: '**',
    component: SignUpComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
